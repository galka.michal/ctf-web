import isEmpty from 'lodash/isEmpty';
import axios from 'axios';


const API = axios.create({
    baseURL: 'https://michalgalka.pl:5000/api/ctf/'
});

export const getBeacons = () => {
    return API.get('beacons/').then(r => r.data).then(data => {
        const features = data.map(beacon => ({
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [beacon.lon, beacon.lat]
            },
            "properties": {
                "owner": beacon.owner
            }
        }))

        return {
            "type": "FeatureCollection",
            "features": features
        }
    })
}

export const getPlayers = () => {
    return API.get('players/').then(r => r.data).then(data => {
        const features = data.map(player => ({
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [player.lon, player.lat]
            },
            "properties": {
                "team": player.team
            }
        }))

        return {
            "type": "FeatureCollection",
            "features": features
        }
    })
}
