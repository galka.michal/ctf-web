import React from "react";
import axios from 'axios';
import { Route } from 'react-router-dom';

import mapboxgl from 'mapbox-gl';
import MapboxClient from 'mapbox';

import { LAYERS, TOKEN } from "./MapConfig";

import beaconsJSON from "./beacons.js";
import playersJSON from "./players.json";

import './styles/Map.less';
import "mapbox-gl/dist/mapbox-gl.css";
import { getPlayers, getBeacons } from "./Api.js";

mapboxgl.accessToken = TOKEN;


export default class Map extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            listItems: [],
            item: null,
            type: null,
            zoom: 15
        }
    }    
    
    componentDidMount() {
        
        this.map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/dark-v9',
            center: [19.992110861732783, 50.06787586727728],
            zoom: this.state.zoom,
        });

        this.map.touchZoomRotate.disableRotation();
        this.map.dragRotate.disable();

        this.map.on('load', (e) => {
            getBeacons().then(data => {
                this.map.addSource(
                    'beacons', {
                        type: 'geojson',
                        data: data
                    });
                this.map.addLayer(LAYERS['beacons']);
                this.map.addLayer(LAYERS['beacons-1']);
                this.map.addLayer(LAYERS['beacons-2']);
                
                var bounds = new mapboxgl.LngLatBounds();
                data.features.forEach(function (feature) {
                    bounds.extend(feature.geometry.coordinates);
                });

                this.map.fitBounds(bounds, {padding: 50});
            });
            
            setInterval(() => {
                // console.log(this.props.state);
                getBeacons().then(data => {
                    this.map.getSource('beacons').setData(data)
                    this.props.updateScore(data)
                })
            }, 3000);
            
            getPlayers().then(data => {
                this.map.addSource(
                    'players', {
                        type: 'geojson',
                        data: data
                    });
                this.map.addLayer(LAYERS['team1']);
                this.map.addLayer(LAYERS['team2']);

                setInterval(() => {
                    getPlayers().then(data => {
                        this.map.getSource('players').setData(data)
                    })
                }, 3000);
            })
        });

    }
    
    render() {
        return (
             <div className="Map" id="map" />
        )
    }
    
}