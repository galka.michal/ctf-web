export default {
    "type": "FeatureCollection",
    "features": [
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    19.992110861732783,
                    50.06787586727728
                ]
            },
            "type": "Feature",
            "properties": {
                "name": "Beacon 1",
                "owner": "team2"
            }
        },
        {
            "geometry": {
                "type": "Point",
                "coordinates": [
                    19.995886845556868,
                    50.068284061761204
                ]
            },
            "type": "Feature",
            "properties": {
                "name": "Beacon 2",
                "owner": "team1"
            }
        }
    ]
}
