
export const TOKEN = "pk.eyJ1Ijoic2ViYXN0aWFuLXBhd2x1cyIsImEiOiJjajQzNDVoNmkwdzdzMzNsZzd1a2N5OTZkIn0.GERywCJOSxHGafQyBuVDnw";

const BeaconSize = 15;
const PlayerSize = 7;
export const Team1Color = "#7FDBFF";
export const Team2Color = "#FF4136";

export const LAYERS = {
    "team1": {
        'id': 'team1',
        'source': 'players',
        'type': 'circle',
        'paint': {
            "circle-color": Team1Color,
            "circle-radius": PlayerSize,
            'circle-stroke-color': '#fff',
            'circle-stroke-width': 1,
        },
        "filter": ["==", "team", 1]
    },
    
    "team2": {
        'id': 'team2',
        'source': 'players',
        'type': 'circle',
        'paint': {
            "circle-color": Team2Color,
            "circle-radius": PlayerSize,
            'circle-stroke-color': '#fff',
            'circle-stroke-width': 1,
        },
        "filter": ["==", "team", 2]
    },
    
    "beacons": {
        'id': 'beacons',
        'source': 'beacons',
        'type': 'circle',
        'paint': {
            "circle-color": "#fff",
            "circle-radius": BeaconSize,
            'circle-stroke-color': '#fff',
            'circle-stroke-width': 1,
        },  
    },
    
    "beacons-1": {
        'id': 'beacons-1',
        'source': 'beacons',
        'type': 'circle',
        'paint': {
            "circle-color": Team1Color,
            "circle-radius": BeaconSize,
            'circle-stroke-color': '#fff',
            'circle-stroke-width': 1,
        },  
        "filter": ["==", "owner", 1]
    },

    "beacons-2": {
        'id': 'beacons-2',
        'source': 'beacons',
        'type': 'circle',
        'paint': {
            "circle-color": Team2Color,
            "circle-radius": BeaconSize,
            'circle-stroke-color': '#fff',
            'circle-stroke-width': 1,
        },  
        "filter": ["==", "owner", 2]
    },

}